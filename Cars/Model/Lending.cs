﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars.Model
{
    public class Lending
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Price { get; set; }
        public int NumberOfDays { get; set; }
        public int CarId { get; set; }
        public virtual Car Car { get; set; }
        public int CustomerID { get; set; }
        public virtual Customer Customer { get; set; } 

    }
}
