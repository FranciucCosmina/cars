﻿using Cars.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars
{
    public class MyDbContext:DbContext
    {
        public MyDbContext(DbContextOptions options):base(options)
        {
            
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Lending> Lendings { get; set; }
        public DbSet<Car> Cars { get; set; }
        protected override void OnModelCreating(ModelBuilder model)
        {
            base.OnModelCreating(model);
            model.Entity<Lending>().HasKey(c => new { c.Id, c.Date });
           
        }
    }
}
