﻿using Cars.Model;
using Cars.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars.Repository
{
    public class CarsRepository : ICarsRepository
    {
        public MyDbContext _context;
        public DbSet<Car> Cars;
        public CarsRepository(MyDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            Cars = _context.Set<Car>();
        }

        public void AddCar(Car car)
        {
            Cars.Add(car);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var car = Cars.FirstOrDefault(c => c.Id == id); 
            Cars.Remove(car);
            _context.SaveChanges();
        }

        public Car GetCar(int id)
        {
            var car = Cars.Include(c => c.Lendings).FirstOrDefault(c => c.Id == id);
            return car;
        }

        public IEnumerable<Car> GetCars()
        {
            return Cars.Include(c => c.Lendings).ToList();
        }

        public void UpdateCar(Car car)
        {
            Cars.Update(car);
            _context.SaveChanges();
        }
    }
}
