﻿using Cars.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars.Repository.Interfaces
{
    public interface ICarsRepository
    {
        Car GetCar(int id);
        IEnumerable<Car> GetCars();

        void AddCar(Car car);

        void UpdateCar(Car car);
        void Delete(int id);
    }
}
