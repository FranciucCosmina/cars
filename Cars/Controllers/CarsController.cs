﻿using AutoMapper;
using Cars.DTO;
using Cars.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars.Controllers
{
    [Route("api/[controller]")]
    public class CarsController:Controller
    {
        public ICarsRepository _carsRepository;
        public IMapper mapper;
        public ILogger<CarsController> _logger;
        public CarsController(ICarsRepository carsRepository,IMapper mapper, ILogger<CarsController>  logger)
        {
            _carsRepository = carsRepository ?? throw new ArgumentNullException(nameof(carsRepository));
            this.mapper = mapper;
            _logger = logger;
        }
        [HttpGet]
        public IEnumerable<CarDTO> GetCars()
        {
            var cars = _carsRepository.GetCars();
            List<CarDTO> carsDto=new List<CarDTO>();
            return mapper.Map<IEnumerable<CarDTO>>(cars);
            
        }
    }
}
