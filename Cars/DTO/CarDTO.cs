﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cars.DTO
{
    public class CarDTO
    {
        public int Id { get; set; }
        public string Brand { get; set; }
    }
}
